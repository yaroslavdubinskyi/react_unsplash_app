import React, { Component } from 'react';


import PhotoPostsGrid from './components/PhotoPostsGrid';
import PhotoModal from './components/PhotoModal';
import UserInfo from './components/UserInfo';

class Photographer extends Component {

  state = {
    p_posts: [],
    perPage: 16,
    currentPage: 1,
    totalPages: 0,
    orderBy: 'latest',
    photoModalOpen: false,
    editedPhoto: {
      src: "https://images.unsplash.com/photo-1432821596592-e2c18b78144f",
      author: 'Dustin Lee'
    },
    userInfo: {
      profile_image: {
        large: ''
      },
      links: {
        html: ''
      }
    },
    settings: {
      contrast: 100,
      hue: 0,
      brightness: 100,
      saturate: 100,
      sepia: 0
    }
  }


  componentDidMount() {
    this.fetchPhotoPosts();
    this.fetchUserInfo();
  }

  fetchPhotoPosts = (isOrdered = false) => {
    // GET /users/:username/photos
    const baseUrl = `https://api.unsplash.com`;
    const username = this.props.match.params.username;
    const orderBy = this.state.orderBy;
    const path = `/users/${username}/photos`;
    const appId = `6c757dbad58a6bf1e56327cbac26cc150f751dffd3ca8113e359106a724316b7`;
    const perPage = this.state.perPage;
    const currentPage = this.state.currentPage;
    const query = `username=${username}&client_id=${appId}&per_page=${perPage}&page=${currentPage}&order_by=${orderBy}`;

    fetch(`${baseUrl}${path}?&${query}`)
      .then(response => {
        const totalPhotos = response.headers.get('X-Total');
        const totalPages = Math.ceil(totalPhotos / this.state.perPage)
        this.setState({
          totalPages
        });
        return response.json();
      })
      .then(data => {
        if(isOrdered){
          this.setState({
            p_posts: data
          });
        }else{
          const old_posts = this.state.p_posts;
          const p_posts = [ ...old_posts, ...data ];
          this.setState({
            p_posts
          });
        }
      })
      .catch(error => console.error(error));
  }

  fetchUserInfo = () => {
    // GET /users/:username
    const baseUrl = `https://api.unsplash.com`;
    const username = this.props.match.params.username;
    const path = `/users/${username}`;
    const appId = `6c757dbad58a6bf1e56327cbac26cc150f751dffd3ca8113e359106a724316b7`;
    const query = `username=${username}&client_id=${appId}`;

    fetch(`${baseUrl}${path}?&${query}`)
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({
          userInfo: data
        });
      })
      .catch(error => console.error(error));
  }

  loadNextPage = () => {
    const new_page = this.state.currentPage + 1;
    this.setState({ currentPage: new_page}, ()=>{
      this.fetchPhotoPosts();
    });

  }

  togglePhotoModal = (img_src, user_info) => {
    const currentState = this.state.photoModalOpen;
    const photo_info = {
      src: img_src,
      author: user_info
    };

    if(img_src){
      this.setState({
        editedPhoto: photo_info,
        photoModalOpen: !currentState
      });
    }else{
      this.setState({
        photoModalOpen: !currentState
      });
    }


  }

  handleSettingChange = event => {
    const setting = event.target.id;
    const value = event.target.value;
    const settings = { ...this.state.settings, [setting]: value };
    this.setState({ settings });
  }

  handleOrderBy = order => {
    this.setState({ orderBy: order }, ()=>{
      this.fetchPhotoPosts(true);
    });
  }

  handleSettingsReset = () => {
    const default_settings = {
      contrast: 100,
      hue: 0,
      brightness: 100,
      saturate: 100,
      sepia: 0
    };
    this.setState({ settings: default_settings });
  }

  render() {
    const { p_posts, photoModalOpen, editedPhoto, settings, totalPages, currentPage, userInfo, orderBy } = this.state;
    return (

      <div className="Photographer">
        <UserInfo userInfo={userInfo}/>
        <PhotoPostsGrid
          photoPosts={p_posts}
          currentPage={currentPage}
          totalPages={totalPages}
          orderBy={orderBy}
          loadNextPage={this.loadNextPage}
          modalToggleHandler={this.togglePhotoModal}
          handleOrderBy={this.handleOrderBy}
        />
        <PhotoModal
          opened={photoModalOpen}
          image={editedPhoto}
          settings={settings}
          handleSettingChange={this.handleSettingChange}
          handleSettingsReset={this.handleSettingsReset}
          modalToggleHandler={this.togglePhotoModal}
        />
      </div>
    );
  }
}

export default Photographer;