import React, { Component } from 'react';


import PhotoPostsGrid from './components/PhotoPostsGrid';
import PhotoModal from './components/PhotoModal';

class Home extends Component {
  state = {
    p_posts: [],
    perPage: 16,
    totalPages: 0,
    currentPage: 1,
    orderBy: 'latest',
    photoModalOpen: false,
    editedPhoto: {
      src: "https://images.unsplash.com/photo-1432821596592-e2c18b78144f",
      author: 'Dustin Lee'
    },
    settings: {
      contrast: 100,
      hue: 0,
      brightness: 100,
      saturate: 100,
      sepia: 0
    }
  }

  componentDidMount() {
    this.fetchPhotoPosts();
  }

  fetchPhotoPosts = (isOrdered = false) => {
    const baseUrl = `https://api.unsplash.com`;
    const path = `/photos`;
    const orderBy = this.state.orderBy;
    const appId = `6c757dbad58a6bf1e56327cbac26cc150f751dffd3ca8113e359106a724316b7`;
    const perPage = this.state.perPage;
    const currentPage = this.state.currentPage;
    const query = `client_id=${appId}&per_page=${perPage}&page=${currentPage}&order_by=${orderBy}`;

    fetch(`${baseUrl}${path}?&${query}`)
      .then(response => {
        const totalPhotos = response.headers.get('X-Total');
        const totalPages = Math.ceil(totalPhotos / this.state.perPage)
        this.setState({
          totalPages
        });
        return response.json();
      })
      .then(data => {
        if(isOrdered){
          this.setState({
            p_posts: data
          });
        }else{
          const old_posts = this.state.p_posts;
          const p_posts = [ ...old_posts, ...data ];
          this.setState({
            p_posts
          });
        }
      })
      .catch(error => console.error(error));
  }

  loadNextPage = () => {
    const new_page = this.state.currentPage + 1;
    this.setState({ currentPage: new_page}, ()=>{
      this.fetchPhotoPosts();
    });

  }

  togglePhotoModal = (img_src, user_info) => {
    const currentState = this.state.photoModalOpen;
    const photo_info = {
      src: img_src,
      author: user_info
    };

    if(img_src){
      this.setState({
        editedPhoto: photo_info,
        photoModalOpen: !currentState
      });
    }else{
      this.setState({
        photoModalOpen: !currentState
      });
    }


  }

  handleSettingChange = event => {
    const setting = event.target.id;
    const value = event.target.value;
    const settings = { ...this.state.settings, [setting]: value };
    this.setState({ settings });
  }

  handleOrderBy = order => {
    this.setState({ orderBy: order }, ()=>{
      this.fetchPhotoPosts(true);
    });
  }

  handleSettingsReset = () => {
    const default_settings = {
      contrast: 100,
      hue: 0,
      brightness: 100,
      saturate: 100,
      sepia: 0
    };
    this.setState({ settings: default_settings });
  }

  render() {
    const { p_posts, photoModalOpen, editedPhoto, settings, totalPages, currentPage, orderBy } = this.state;
    return (

      <div className="Home">

        <PhotoPostsGrid
          photoPosts={p_posts}
          currentPage={currentPage}
          totalPages={totalPages}
          orderBy={orderBy}
          loadNextPage={this.loadNextPage}
          modalToggleHandler={this.togglePhotoModal}
          handleOrderBy={this.handleOrderBy}
        />
        <PhotoModal
          opened={photoModalOpen}
          image={editedPhoto}
          settings={settings}
          handleSettingChange={this.handleSettingChange}
          handleSettingsReset={this.handleSettingsReset}
          modalToggleHandler={this.togglePhotoModal}
        />
      </div>
    );
  }
}

export default Home;