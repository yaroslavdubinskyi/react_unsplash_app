import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import './sass/main.scss';

import AppHeader from './components/Header';
import Footer from './components/Footer';

// Pages
import Home from './Home';
import Photographer from './Photographer';

class App extends Component {

  render() {
    const baseUrl = process.env.PUBLIC_URL;
    return (
      <div className="App">

        <Router>
          <div>
            <AppHeader/>
            <Route exact path={baseUrl + "/"} component={Home}/>
            <Route path={baseUrl + "/user/:username"} component={Photographer}/>
            <Footer/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
