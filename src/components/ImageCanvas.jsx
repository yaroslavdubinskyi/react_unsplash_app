import React, { Component } from 'react';

class ImageCanvas extends Component {
  componentDidMount() {
    this.updateCanvas();
  }
  componentWillReceiveProps(){
    this.updateCanvas();
  }
  updateCanvas = () => {
    const settings = this.props.settings;
    const canvas = this.refs.canvas;
    const ctx = canvas.getContext('2d');

    const image = this.refs.image;



    ctx.filter = settings;

    ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

  }
  render() {
    const img_style =  {
      display: 'none'
    };
    return (
      <div>
        <img src={this.props.image} ref="image" alt="" onLoad={this.updateCanvas} style={img_style}/>
        <canvas ref="canvas" width="800" height="600" />
      </div>

    );
  }
}

export default ImageCanvas;