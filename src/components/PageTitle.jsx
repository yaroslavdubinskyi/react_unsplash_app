import React from 'react';

function PageTitle({title}) {
  return(
    <div className="pageTitle">
      <div className="container">
        <h1>{title}</h1>
      </div>
    </div>
  )
}

export default PageTitle;