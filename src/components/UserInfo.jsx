import React from 'react';

function UserInfo({userInfo}) {
  const username = userInfo.username;
  const fullname = userInfo.name;
  const totalPhotos = userInfo.total_photos;
  const totalLikes = userInfo.total_likes;
  const downloads = userInfo.downloads;
  const userpic = userInfo.profile_image;
  const userHtmlLink = userInfo.links.html;
  return(
    <div className="container">
      <div className="user-info-wrap">
        <img src={userpic.large} alt=""/>
        <p className="fullname">{fullname}</p>
        <p className="username"><a href={userHtmlLink}>Unsplash - @{username}</a></p>
        <p className="location">{userInfo.location}</p>
        <div className="user-stats-wrap">
          <div className="stat-item">
            Total photos:
            <span>{totalPhotos}</span>
          </div>
          <div className="stat-item">
            Total likes:
            <span>{totalLikes}</span>
          </div>
          <div className="stat-item">
            Downloads:
            <span>{downloads}</span>
          </div>
        </div>
      </div>
    </div>

  )
}

export default UserInfo;