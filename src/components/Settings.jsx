import React from 'react';
import Range from './Range';

function Settings({settings, handleSettingChange, handleSettingsReset}) {

  const { contrast, hue, brightness, saturate, sepia } = settings;

  return(
    <div className="settings-wrap">
      <p className="title">Settings:</p>

      <Range
        name="contrast"
        value={contrast}
        min={0}
        max={200}
        onChange={handleSettingChange} />

      <Range
        name="hue"
        value={hue}
        min={-360}
        max={360}
        onChange={handleSettingChange} />

      <Range
        name="brightness"
        min={0}
        max={200}
        value={brightness}
        onChange={handleSettingChange} />

      <Range
        name="saturate"
        min={0}
        max={100}
        value={saturate}
        onChange={handleSettingChange} />

      <Range
        name="sepia"
        min={0}
        max={100}
        value={sepia}
        onChange={handleSettingChange} />

      <div className="buttons-wrap">
        <button className="button" onClick={handleSettingsReset}>Reset settings</button>
        <button className="button" >Download</button>
      </div>
    </div>
  )
}

export default Settings;