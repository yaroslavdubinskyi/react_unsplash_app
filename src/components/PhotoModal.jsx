import React from 'react';
import Settings from './Settings';
import ImageCanvas from './ImageCanvas';

function getFilterCSSStyles(settings) {
  let filterString = '';

  for (let prop in settings) {
    if (settings.hasOwnProperty(prop)) {
      switch (prop) {
        case 'hue':
          filterString += 'hue-rotate(' + settings[prop] + 'deg) ';
          break;
        default:
          filterString += prop + '(' + settings[prop] + '%) ';
      }
    }
  }

  return filterString;
}

function PhotoModal({opened, image, settings, handleSettingChange, handleSettingsReset, modalToggleHandler}) {

  const filter = getFilterCSSStyles(settings);

  const modalOpened = opened;
  const imageSrc = image.src;
  const imageAuthor = image.author;

  let classes = "photo-modal ";
  if(modalOpened){
    classes += "opened";
  }
  
  return(
    <div className={classes}>
      <div className="popup-overlay" onClick={() => modalToggleHandler(false)}></div>
      <div className="popup-wrap">
        <button className="toggle-button" onClick={() => modalToggleHandler(false)}>×</button>
        <div className="popup-inner">
          <div className="col col-left">
            <ImageCanvas
              image={imageSrc}
              settings={filter}
            />
            <div className="desc-wrap">
              <p>{imageAuthor} / Unsplash</p>
            </div>
          </div>
          <div className="col col-right">
            <Settings
              settings={settings}
              handleSettingChange={handleSettingChange}
              handleSettingsReset={handleSettingsReset}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default PhotoModal;