import React from 'react';

function OrderBy({current, handleOrderBy}) {

  const currentOrder = current;

  return(
    <div className="orderby-wrap">
      <ul>
        <li className={ currentOrder === 'latest' ? 'item current' : 'item'} onClick={()=> handleOrderBy('latest')}>Latest</li>
        <li className={ currentOrder === 'oldest' ? 'item current' : 'item'} onClick={()=> handleOrderBy('oldest')}>Oldest</li>
        <li className={ currentOrder === 'popular' ? 'item current' : 'item'} onClick={()=> handleOrderBy('popular')}>Popular</li>
      </ul>
    </div>
  )
}

export default OrderBy;