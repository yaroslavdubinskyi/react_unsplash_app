import React from 'react';
import { Link } from 'react-router-dom';

function PhotoPost({item, modalToggleHandler}) {
  const p_likes = item.likes;
  const p_image_src = item.urls.regular;
  const p_image_raw = item.urls.raw;
  const p_image_height = item.height;
  const p_image_width = item.width;
  const p_description = item.description;
  const p_user_info = item.user;
  const className = `item${p_image_height > p_image_width ? ' horizontal' : ''}`;

  return (
    <article className={className}>
      <img className="post-photo" src={p_image_src} alt={p_description} onClick={()=>modalToggleHandler(p_image_raw, p_user_info.name)} />
      <span className="likes">Likes: {p_likes}</span>
      <div className="desc-wrap">
        <Link to={'/user/'+p_user_info.username}>
          <img className="author-pic" src={p_user_info.profile_image.medium} alt={p_user_info.name} />
          <p className="author-name">{p_user_info.name} / Unsplash</p>
        </Link>
      </div>
    </article>
  );
}

export default PhotoPost;