import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import OrderBy from './OrderBy';
import PhotoPost from './PhotoPost';

const FadeUp = ({ children, ...props }) => (
  <CSSTransition
    {...props}
    timeout={600}
    classNames="fade-up"
  >
    {children}
  </CSSTransition>
);

function PhotoPostsGrid({photoPosts, modalToggleHandler, loadNextPage, totalPages, currentPage, orderBy, handleOrderBy}) {
  const photos_items = photoPosts.map((item, index) =>
    <FadeUp key={item.id.toString()}>
      <PhotoPost item={item} modalToggleHandler={modalToggleHandler} />
    </FadeUp>
  );
  let isPagination = false;
  if(totalPages > currentPage){
    isPagination = true;
  }

  return (
    <div className="photos-grid-wrap">
      <div className="container">
        <OrderBy current={orderBy} handleOrderBy={handleOrderBy}/>
        <TransitionGroup className="photos-transition photos-grid grid">
          {photos_items}
          <div className={isPagination ? 'pagination-wrap' : 'pagination-wrap hidden'}>
            <button className="button" onClick={loadNextPage}>Load more</button>
          </div>
        </TransitionGroup>
      </div>
    </div>
  );
}

export default PhotoPostsGrid;