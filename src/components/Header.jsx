import React from 'react';
import { Link } from 'react-router-dom';

function AppHeader() {
  return(
    <header className="App-header">
      <div className="container">
        <div className="header-inner">
          <div className="logo">
            <Link to="/">
              <h1 className="App-title">Unsplash React Demo</h1>
            </Link>
          </div>
          <nav className="nav-wrap">
            <ul>
              <li><Link to="/">Home</Link></li>
              <li>Collections</li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  )
}

export default AppHeader;