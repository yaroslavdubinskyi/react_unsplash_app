import React from 'react';

function Footer() {
  return(
    <footer className="app-footer">
      <div className="container">
        Simple app to learn ReactJS <br/>
        <a href="https://unsplash.com" target="_blank" rel="noopener noreferrer">unsplash.com</a>
      </div>
    </footer>
  )
}

export default Footer;